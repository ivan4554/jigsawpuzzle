﻿using System.Collections;
using UnityEngine;

// Класс JigsawMain используется для доступа к (blender - fbx) импортированным прототипам деталей и // для доступа к материалам для линий, образца изображения, разбросанных частей и оснований частей. // // Вам нужно прикрепить этот скрипт к объекту в вашей сцене. Самый простой способ — добавить префаб JigsawMain // в вашу сцену.
//
namespace CodeBase
{
    public class JigsawMain: MonoBehaviour {
    
        public GameObject JigsawGrid;  		// будет содержать импортированный файл блендера FBX со всеми (25*9) кусочками головоломки
        [Header("Materials")]
        public Material LinesHorizontal;	// материал для горизонтальных линий
        public Material LinesVertical;	// материал для вертикальных линий
        public Material SampleImage;		// материал для образца изображения
        public Material ScatteredPieces;	// материал для разбросанных кусочков
        public Material PieceBase;		// материал для штучных оснований
        
        public int LayerMask = 31;				// маска слоя по умолчанию для быстрого RayCasting
	
        private readonly Hashtable _basePieceTransform = new Hashtable();
        private bool _isValid = false;

        // верно, если этот класс был правильно инициализирован
        public bool IsValid => _isValid;
    
        // загружаем ссылки на все части головоломки Преобразование объектов в HashTable
        void Start () => 
            GetBasePieces();

        // получаем прототип базовой части
        public Transform GetBase(string ident) =>
            JigsawGrid.transform.Find(ident); // todo Refactor
	
        // получаем конкретный прототип детали
        public Transform GetPiece(string ident, string piece)
        {
            Transform basePiece = _basePieceTransform[ident] as Transform;
            
            if (basePiece)
                return basePiece.Find(ident + piece.ToUpper()); // todo Refactor
        
            return null;
        }

        // Загружаем все 25 базовых частей головоломки в HashTable
        private void GetBasePieces()
        {
            bool aPieceNotFound = false;
            for (int px = 1; px <= 5; px++)
            {
                for (int py = 1; py <= 5; py++)
                {
                    string ident = "" + px + "" + py;
                    Transform t = GetBase(ident);
                    if (t == null)
                        aPieceNotFound = true;
                    _basePieceTransform.Add(ident, t);
                }
            }
            _isValid = !aPieceNotFound;
        }


    }
}
