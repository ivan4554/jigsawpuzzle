﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Класс JigsawPuzzle — это базовый класс, который можно использовать для создания внутриигровой головоломки.
//
// обычной процедурой было бы создание пользовательского подкласса JigsawPuzzle.    								
// и переопределить следующие базовые функции:                               										 	
//
// - PuzzleStart(); 						: вызывается при запуске новой головоломки       							
// - ActivatePiece(GameObject piece); 		: вызывается, когда выбран «свободный» фрагмент головоломки (начать перетаскивание)      
// - DeactivatePiece(GameObject piece);		: вызывается, когда отпускается «свободный» фрагмент головоломки (остановить перетаскивание) 	   
// - PiecePlaced(GameObject piece); 		: вызывается, когда часть головоломки помещается на головоломку в нужном месте     
// - PuzzleSolved(int moves, float time);	: вызывается, когда головоломка решена						
// 
// (см. класс DemoJigsawPuzzle, включенный в демо-версию продукта)													
//
namespace CodeBase
{
	[System.Serializable]
	public class JigsawPuzzle : MonoBehaviour {

		// ---------------------------------------------------------------------------------------------------------
		// общедоступные (опубликованные) атрибуты — их можно установить после
		// добавления скрипта в контейнер (например, примитив куба)
		// ---------------------------------------------------------------------------------------------------------

		public Texture image = null;                // будет содержать спроецированное изображение мозаики                                                						
		public Vector2 size = new Vector2(5,5);     // сколько частей будет в этой головоломке (x,y)								 size piece
		public string topLeftPiece = "11";          // верхняя левая часть - формат YX (1,2,3,4,5), поэтому от 11 до 55 - 25 уникальных начальных возможностей   
		public bool outsideSnapping = true;			// соединяет совпадающие части вместе за пределами области сетки					  		
		public bool showImage = true;               // отображаем полупрозрачное полупрозрачное изображение "помощника" в оттенках серого			
		public bool showLines = true;               // отображаем вспомогательную матрицу головоломки 										
		public int placePrecision = 12;             // насколько точным должен быть элемент, помещаемый в пазл (= процент | 6-15 являются хорошими значениями, может быть выше до 25 для маленьких детей)
		public bool scatterPieces = true;			// разбрасывать кусочки, когда начинается головоломка - если false, части размещаются на своих местах в головоломке  
		public float spacing = 0.01f;      																			

		bool rotatePieces = false;			        // true для добавления поддержки поворота - ЕЩЕ НЕ РЕАЛИЗОВАНО
	
		// ---------------------------------------------------------------------------------------------------------
		// защищенные атрибуты (доступны в подклассе custom)
		// ---------------------------------------------------------------------------------------------------------

		// количество частей текущего пазла
		protected int pieceCount => (int)(size.x * size.y);

		// количество частей, помещенных в текущую головоломку
		protected int piecesPlaced => puzzleContainer.transform.childCount;

		// количество частей, оставшихся в текущей головоломке
		protected int piecesScattered => piecesContainer.transform.childCount;

		// прогресс головоломки - процент 0-100
		protected float puzzleProgress => (piecesPlaced/pieceCount) * 100;

		private JigsawMain main = null;	
		private bool checkedOnce = false;			
		private int puzzleMode = 0;					
		private GameObject linesH = null;		
		private GameObject linesV = null;			
		private GameObject sampleImage = null;		
		private GameObject piecesContainer = null;	
		private GameObject puzzleContainer = null;
		private GameObject pieceCache = null;
		private GameObject puzzlePlane = null;
		private int puzzleMoves = 0;
		private int puzzleTicks = 0;
		private bool restarting = false;
		private bool dragging = false;
		private Touch dragTouch;
    
		private GameObject activePiece = null;
		private Vector3 activePoint;

		private Vector2 checkSize;
		private Vector2 checkContainerSize;
		private string  checkTopLeftPiece = "";

		private Hashtable piecesLookup = new Hashtable();
		private Hashtable piecePositions = new Hashtable();
	
		private Dictionary<GameObject,List<GameObject>> connectedPieces = new Dictionary<GameObject, List<GameObject>>();
		private GameObject[,] neighborGrid;
		private Dictionary<GameObject,Vector2> neighborGridLookup = new Dictionary<GameObject, Vector2>();

		private List<GameObject> pieces = new List<GameObject>();

		// ---------------------------------------------------------------------------------------------------------
		// virtual methods
		// ---------------------------------------------------------------------------------------------------------
	
		// Это будет вызвано, когда будет запущена новая головоломка.
		// вам нужно переопределить этот метод, когда вы создаете свой «пользовательский» подкласс
		protected virtual void PuzzleStart()
		{
		}
	
		// Это будет вызвано, когда будет выбран «свободный» фрагмент головоломки (начать перетаскивание).
		// вам нужно переопределить этот метод, когда вы создаете свой «пользовательский» подкласс
		protected virtual void ActivatePiece(GameObject piece)
		{
		}
    
		// This will be called when a puzzle piece has been released (stop drag)
		// you need to override this method when you create your 'custom' subclass
		protected virtual void DeactivatePiece(GameObject piece)
		{
		}
	
		
		// Это будет вызвано, когда часть головоломки будет освобождена (остановить перетаскивание)
		// вам нужно переопределить этот метод, когда вы создаете свой «пользовательский» подкласс
		protected virtual void PiecePlaced(GameObject piece)
		{
		}
	
		
		// Это будет вызвано, когда головоломка будет решена
		// вам нужно переопределить этот метод, когда вы создаете свой «пользовательский» подкласс
		protected virtual void PuzzleSolved(int moves, float time)
		{
		}	

		/// <summary>
		/// Будет вызываться, когда кусок будет защелкнут снаружи
		/// </summary>
		protected virtual void SnappedOutside(GameObject piece,GameObject[] chain){
		
		}
	
	
		// это будет вызвано, когда кусок вот-вот будет разбросан - если возвращается null, выполняется разбрасывание по умолчанию
		// вам нужно переопределить этот метод, когда вы создаете свой «пользовательский» подкласс
		protected virtual GameObject ScatterPiece(GameObject piece) => null;

		// ---------------------------------------------------------------------------------------------------------
		// methods
		// ---------------------------------------------------------------------------------------------------------
	
		// перезапустить текущую головоломку
		// установить индикатор того, что головоломка должна быть перезапущена - это подхватывается в процессе Update()
		public void Restart() => restarting = true;

		// Обновление вызывается один раз за кадр
		protected void Update () {
			if (main == null)
			{
				// инициализация основной головоломки
				if (!checkedOnce)
				{
					GameObject go = GameObject.Find("JigsawMain");
					if (!go)
					{
						Debug.LogError("JigsawMain (prefab) GameObject not added to scene!");
						checkedOnce = true;
						return;
					}
					main = go.GetComponent("JigsawMain") as JigsawMain;
					if (main != null)
						if (!main.IsValid)
						{
							Debug.LogError("JigsawMain (prefab) GameObject is not valid - Base puzzle pieces could not be found!");
							main = null;
							checkedOnce = true;
							return;
						}

					// проверяем, существует ли TopLeftPiece, если нет, возьмем '11'
					if (main.GetBase(topLeftPiece) == null)
						topLeftPiece = "11";

					// инициализация этой головоломки
					// создание горизонтальных и вертикальных линий
					SetLines();
					// создаем образец изображения
					SetSample();
					// создаем кусочки этой головоломки
					SetPieces(false);
					// создаем плоскость управления мышью
					SetPlane();
				}
				else
					// система головоломок недействительна, поэтому return;
					return;
			}
			else
			{
				// JigsawMain найден и действителен, так что мы можем продолжить нашу головоломку	
				// Давайте сначала проверим, были ли изменены базовые настройки головоломки, такие как размер или верхняя левая часть, поэтому мы должны принудительно перезапустить
				if (!Vector2.Equals(size, checkSize) || topLeftPiece!=checkTopLeftPiece || restarting)
				{
					if (activePiece)
					{
						// деактивировать текущую активную фигуру
						DeactivatePiece(activePiece);
						activePiece = null;
					}
					// настройки базовой головоломки были изменены, поэтому сбрасываются линии, образец изображения и части.
					SetLines();
					SetSample();
					SetPieces(true);
					// перезапуск головоломки - поэтому PuzzleMode в 0
					puzzleMode = 0;
				}

				// проверяем, нужно ли отображать/скрывать линии
				if (linesH.activeSelf != showLines) linesH.SetActive(showLines);
				if (linesV.activeSelf != showLines) linesV.SetActive(showLines);
				// проверяем, нужно ли отображать/скрывать образец изображения
				if (sampleImage.activeSelf != showImage) sampleImage.SetActive(showImage);

				// Управление головоломкой
				switch (puzzleMode)
				{
					case 0:     // инициализация головоломки
						if (pieceCount == 0) return;
				
						if (scatterPieces) 
							// у нас есть кусочки, так что разбросайте их
							ScatterPieces();
				
						if (rotatePieces)
							// вращаем фигуры на разбросанном месте
							RotatePieces();
				
						// начинаем собирать пазлы, поэтому сбрасываем счетчик ходов и время головоломки
						puzzleMoves = 0;
						puzzleTicks = System.Environment.TickCount;
						restarting = false;
						// вызов переопределенной функции PuzzleStart
						PuzzleStart();
						// Управление головоломкой до следующего шага
						puzzleMode++;
						break;
								
					case 1:     // мы ломаем голову

						if (Input.GetMouseButton(0))
						{										
							// регистрируем первое касание
							if (Input.touchCount>0 && !dragging)
								dragTouch = Input.touches[0];
	
							if (Input.touchCount==0 || (Input.touchCount>0 && dragTouch.fingerId == Input.touches[0].fingerId))
							{
								// проверяется только позиция щелчка мыши или первого касания
								Vector3 position = Input.mousePosition;
								if (Input.touchCount>0)
									position = Input.touches[0].position;
								// левая кнопка мыши нажата, поэтому проверяем, есть ли у нас активная фигура
								if (activePiece != null)
								{
									// если у нас есть активная фигура, мы будем двигать ее, если мы двигали мышь
									RaycastHit hit;
									// направляем луч из камеры на плоскость управления мышью
									if (puzzlePlane.GetComponent<Collider>().Raycast(Camera.main.ScreenPointToRay(position), out hit,Vector3.Distance(Camera.main.transform.position, transform.position)*2))
									{
										// вычисляем расстояние между предыдущим hit.point и текущим
										Vector3 d = hit.point - activePoint;
										// переместить активную фигуру на рассчитанное расстояние
										activePiece.transform.position += d;
								
										//Также перемещаем все в подключенной цепочке
										if(connectedPieces.ContainsKey(activePiece)){
											foreach(GameObject piece in connectedPieces[activePiece]){
												piece.transform.position += d;
											}
										}
								
										// текущая точка становится новой hit.point															
										activePoint = hit.point;
									}
								}
								else
								{
									// нет активной фигуры, поэтому проверяем, можем ли мы ее получить
									// отправляем луч из камеры, собираем все попадания, которые соответствуют маске слоя «головоломка» (31 = по умолчанию, но может быть установлено в классе JigSawMain)
									RaycastHit[] hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(position),Vector3.Distance(Camera.main.transform.position, transform.position)*2, 1 << main.LayerMask);
									if (hits.Length>0)
									{
										// мы что-то наткнулись, так что проверяйте
										for (int h=0; h<hits.Length; h++)
										{
											if (hits[h].collider.gameObject == puzzlePlane)
												// если мы попадаем в плоскость управления мышью, мы регистрируем hit.point для перемещения фигуры
												activePoint = hits[h].point;
											else
											if (hits[h].collider.gameObject.transform.parent.gameObject == piecesContainer)
											{
												// мы ударили фигуру, поэтому делаем ее активной, если ее нет или если она «вперед» на текущую активную фигуру
												if ((activePiece!=null && activePiece.transform.position.z >  hits[h].collider.gameObject.transform.position.z) || activePiece==null)
													activePiece = hits[h].collider.gameObject;
											}
										}
										// активируем кусок, если мы его нашли
										if (activePiece != null)
											ActivatePiece(activePiece);
								
										if(activePiece != null){
											if(connectedPieces.ContainsKey(activePiece)){
												foreach(GameObject piece in connectedPieces[activePiece]){
													ActivatePiece(piece);
												}
											}
										}
								
										dragging = true;
									}
								}
							}
						}
						else
						{
							dragging = false;
							// левая кнопка мыши не нажата
							if (activePiece != null)
							{
								// если у нас есть активная фигура, мы должны ее деактивировать.
								DeactivatePiece(activePiece);
							
								if(connectedPieces.ContainsKey(activePiece)){
									foreach(GameObject piece in connectedPieces[activePiece]){
										DeactivatePiece(piece);
									}
								}
						
								// увеличить количество ходов
								puzzleMoves++;
						
								if (PieceInPlace())
								{
									// деталь высвобождается в нужном месте, поэтому зафиксируйте ее на месте
									activePiece.transform.position = PiecePosition((Vector2)piecePositions[activePiece.name]);
									// устанавливаем parent в PuzzleContainer, чтобы мы заблокировали его для перетаскивания
									activePiece.transform.parent = puzzleContainer.transform;
									// вызов переопределенной функции PiecePlaced
									PiecePlaced(activePiece);
							
							
									// также защелкиваем все соединенные части на место
									if(connectedPieces.ContainsKey(activePiece)){
										foreach(GameObject piece in connectedPieces[activePiece]){
									
											piece.transform.position = PiecePosition((Vector2)piecePositions[piece.name]);
											piece.transform.parent = puzzleContainer.transform;
											PiecePlaced(piece);
											connectedPieces.Remove(piece);
										}
								
										connectedPieces.Remove(activePiece);
									}
							
							
									if (puzzleProgress == 100)
									{
										// головоломка решена, поэтому вызовите переопределенную функцию PuzzleSolved
										PuzzleSolved(puzzleMoves, (System.Environment.TickCount - puzzleTicks) / 1000);
										puzzleMode++;
									}
                                
								}
								else{
									List<GameObject> neighborChain = CheckNeighbors();
									if(neighborChain != null && outsideSnapping){
										foreach(GameObject go in neighborChain){
											Debug.Log("n: "+go.name);
										}
										neighborChain.Add(activePiece);
										if(connectedPieces.ContainsKey(activePiece)){
											neighborChain.AddRange(connectedPieces[activePiece]);
										}
									
										foreach(GameObject piece in neighborChain){
										
											List<GameObject> chain = new List<GameObject>();
											chain.AddRange(neighborChain);
											chain.Remove(piece);
											if(connectedPieces.ContainsKey(piece)){
												connectedPieces[piece] = chain;
											}else{
												connectedPieces.Add(piece,chain);
											}
											chain = new List<GameObject>();//мы создаем новый список, чтобы не редактировать connectPieces[piece]
											chain.AddRange(connectedPieces[piece]); 
											chain.Add(piece);

										}
								
										//привязать все части к activePiece
										neighborChain.Remove(activePiece);
										foreach(GameObject piece in neighborChain){
											Vector3 diffDist = PiecePosition((Vector2)piecePositions[piece.name]) - PiecePosition((Vector2)piecePositions[activePiece.name]);
											piece.transform.position = activePiece.transform.position + diffDist;
										}
								
										SnappedOutside(activePiece,neighborChain.ToArray());
									}
							
								}
								activePiece = null;
							}
						}

						break;
					case 2:     // Головоломка Готова - это своего рода состояние сна.
						break;                        
				}
			}	
		}
	
		// Определяем, находится ли активная фигура в нужном месте.
		// настройка placePrecision говорит нам, насколько точно мы должны подогнать фрагмент головоломки
		// 12-15 — хорошая настройка
		// 15-25 для маленьких детей
		private bool PieceInPlace()
		{
			// вызов размера базовой детали
			float dX = (transform.localScale.x / size.x);
			float dY = (transform.localScale.y / size.y);
			// рассчитываем вектор расстояния между правильной позицией активной фигуры и текущей позицией
			Vector3 dV = PiecePosition((Vector2)piecePositions[activePiece.name]) - activePiece.transform.position;
			// управляющий вектор имеет размер куска
			Vector3 cV = new Vector3(dX, dY, 0);
			// проверка расстояния (с точностью до места) от текущей позиции до правильной позиции
			return ((Vector3.Distance(Vector3.zero, dV) / Vector3.Distance(Vector3.zero, cV) * 100) < placePrecision);
		}
	
	
		private List<GameObject> CheckNeighbors(){
			//мы используем это для поиска потенциальных соседей, затем очищаем его и используем для возврата цепочки
			List<GameObject> neighbors = new List<GameObject>();
			Vector2 piecePos = neighborGridLookup[activePiece];
		
			//получаем уже установленную цепочку
			List<GameObject> alreadyConnected = new List<GameObject>();
			if(connectedPieces.ContainsKey(activePiece)){
				alreadyConnected = connectedPieces[activePiece];
			}
		
			//проверка по краям
			if(piecePos.x > 1){
				GameObject neighbor = neighborGrid[(int)piecePos.x-1,(int)piecePos.y];
				if(!alreadyConnected.Contains(neighbor)) neighbors.Add(neighbor);
			}
			if(piecePos.x < size.x){
				GameObject neighbor = neighborGrid[(int)piecePos.x+1,(int)piecePos.y];
				if(!alreadyConnected.Contains(neighbor)) neighbors.Add(neighbor);
			}
			if(piecePos.y > 1){
				GameObject neighbor = neighborGrid[(int)piecePos.x,(int)piecePos.y-1];
				if(!alreadyConnected.Contains(neighbor)) neighbors.Add(neighbor);
			}
			if(piecePos.y < size.y){
				GameObject neighbor = neighborGrid[(int)piecePos.x,(int)piecePos.y+1];
				if(!alreadyConnected.Contains(neighbor)) neighbors.Add(neighbor);
			}
		
		
			if(neighbors.Count == 0 ){
				Debug.Log("no neighbors");
				return null;
			} 
		
			//для каждого потенциального соседа, находим того, к которому мы ближе всего
			GameObject closest = neighbors[0];
			float dist = Vector3.Distance(neighbors[0].transform.position,activePiece.transform.position);
			if(neighbors.Count > 1){
				for(int cnt = 1; cnt < neighbors.Count; cnt++){
					float d = Vector3.Distance(neighbors[cnt].transform.position,activePiece.transform.position);
					if(d < dist){
						dist = d;
						closest = neighbors[cnt];
					}
				}
			}
		
			//проверяем правильность ближайшего соседа, используя логику PieceInPlace()
			float dX = (transform.localScale.x / size.x);
			float dY = (transform.localScale.y / size.y);
			Vector3 dV = closest.transform.position - activePiece.transform.position;
			Vector3 dV2 = PiecePosition((Vector2)piecePositions[closest.name]) - PiecePosition((Vector2)piecePositions[activePiece.name]);
			Vector3 cV = new Vector3(dX, dY, 0);
		
			float validDist = (Vector3.Distance(Vector3.zero, dV-dV2) / Vector3.Distance(Vector3.zero, cV) * 100);
			Debug.Log(validDist + " : " + placePrecision);
			bool valid = (validDist < placePrecision);
		
			if(!valid) return null;
		
			//добавляем предсуществующую цепочку
			neighbors.Clear();
			neighbors.Add(closest);
			if(connectedPieces.ContainsKey(closest)){
				neighbors.AddRange(connectedPieces[closest]);
			}
			return neighbors;
		
		
		}
	
		// Создаем горизонтальные линии для отображения на головоломке
		private void SetLinesHorizontal()
		{
			// у нас должен быть правильный topLeftPiece
			if (topLeftPiece.Length != 2) return;
			// получаем начальную x-линию из верхней левой части
			int tpX = System.Convert.ToInt32(topLeftPiece.Substring(1, 1));
			// получить начальную y-линию из верхней левой части
			int tpY = System.Convert.ToInt32(topLeftPiece.Substring(0, 1));
			// мы создадим заново, поэтому уничтожим, если у нас уже есть строки
			if (linesH != null) GameObject.Destroy(linesH);
			// создаем кубический примитив для этих линий
			linesH = GameObject.CreatePrimitive(PrimitiveType.Cube);
			linesH.name = "lines-horizontal";
			// добавляем линии в пазл
			linesH.transform.parent = gameObject.transform;
			// установить «прозрачный» материал на горизонтальные линии
			linesH.GetComponent<Renderer>().material = main.LinesHorizontal;
			// установите правильный масштаб (z = очень тонкий), вращение и положение, чтобы он закрывал головоломку
			linesH.transform.localScale = new Vector3(-1, -1 * (1 / size.y) * (size.y - 1), 0.0001F);
			linesH.transform.rotation = transform.rotation;
			// переместите этот «тонкий» куб так, чтобы он парил прямо над головоломкой
			linesH.transform.position = transform.position +
			                            transform.forward * ((transform.localScale.z / 2) + 0.001F);
			// масштабируем текстуру по отношению к указанному размеру
			linesH.GetComponent<Renderer>().material.mainTextureScale = new Vector2(-0.2F * size.x, -0.2F * (size.y - 1));
			// устанавливаем правильное смещение по отношению к указанному размеру и указанному topLeftPiece
			linesH.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(((5 - size.x) * -0.2F) + ((tpX - 1) * 0.2F), 0.005F + ((tpY - 1) * -0.2F));
			linesH.SetActive(false);
		}
	
		// Создаем вертикальные линии для отображения на головоломке
		private void SetLinesVertical()
		{
			// у нас должен быть правильный topLeftPiece
			if (topLeftPiece.Length != 2) return;
			// получаем начальную x-линию из верхней левой части
			int tpX = System.Convert.ToInt32(topLeftPiece.Substring(1, 1));
			// получить начальную y-линию из верхней левой части
			int tpY = System.Convert.ToInt32(topLeftPiece.Substring(0, 1));
			// мы создадим заново, поэтому уничтожим, если у нас уже есть строки
			if (linesV != null) GameObject.Destroy(linesV);
			// создаем примитив куба для этих строк
			linesV = GameObject.CreatePrimitive(PrimitiveType.Cube);
			linesV.name = "lines-vertical";
			// добавляем линии в пазл
			linesV.transform.parent = gameObject.transform;
			// установить «прозрачный» материал на линии горизонта
			linesV.GetComponent<Renderer>().material = main.LinesVertical;
			// установите правильный масштаб (z = очень тонкий), вращение и положение, чтобы он закрывал головоломку
			linesV.transform.localScale = new Vector3(-1 * (1 / size.x) * (size.x - 1), -1, 0.0001F);
			linesV.transform.rotation = transform.rotation;
			// переместите этот «тонкий» куб так, чтобы он парил прямо над головоломкой
			linesV.transform.position = transform.position +
			                            transform.forward * ((transform.localScale.z / 2) + 0.001F);
			// масштабируем текстуру по отношению к указанному размеру
			linesV.GetComponent<Renderer>().material.mainTextureScale = new Vector2(-0.2F * (size.x - 1), -0.2F * size.y);
			// устанавливаем правильное смещение по отношению к указанному размеру и указанному topLeftPiece
			linesV.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(-0.2F * ((5 - size.x) + 1) + ((tpX - 1) * 0.2F), 0 + +((tpY - 1) * -0.2F));
			linesV.SetActive(false);
		}

		private void SetLines()
		{
			// создаем линии головоломки
			SetLinesHorizontal();
			SetLinesVertical();
			// сохраняем размер и верхнюю левую часть, чтобы мы могли принудительно перезапустить, если они изменятся
			checkSize = size;
			checkTopLeftPiece = topLeftPiece;
		}
	
		// вращаем фигуры на месте их разброса
		private void RotatePieces()
		{
			// мы будем использовать координаты мирового пространства и масштаб головоломки для расположения частей
			Vector3 s = transform.localScale;
			float dX = s.x / size.x;
			float dY = s.y / size.y;
			for (int p = 0; p<pieces.Count; p++)
			{
				GameObject piece = pieces[p] as GameObject;
				piece.transform.parent = null;
				piece.transform.RotateAround(piece.transform.position+transform.right * (-1 * (dX/2)) +transform.up * (-1 * (dY/2)),transform.forward,Random.value * 360);
			}
		}
   
		// разбрасываем кусочки и размещаем их случайным образом вокруг головоломки
		private void ScatterPieces()
		{
			ArrayList piecesToScatter = new ArrayList(pieces);
			while (piecesToScatter.Count > 0)
			{
				// берём случайный кусок
				GameObject piece = piecesToScatter[(int)(Mathf.Floor(Random.value * piecesToScatter.Count))] as GameObject;
				piecesToScatter.Remove(piece);
			
				// сначала попробуйте пользовательскую функцию разброса
				if (ScatterPiece(piece)!=null)
					continue;
			
				// будет определять, в каком прямоугольнике может быть рассчитана случайная позиция для этой фигуры
				Rect r = new Rect();
			
				// мы будем использовать координаты мирового пространства и масштаб головоломки для расположения частей
				Vector3 p = transform.position;
				Vector3 s = transform.localScale;
				float dX = s.x / size.x;
				float dY = s.y / size.y;
			
				// si будет содержать наименьший размер области разброса (высота/ширина головоломки, разделенная на 4 `)
				var si = s.x / 4;
				if (s.y / 4 < si) si = s.y / 4;
			
				// случайным образом определяем, должна ли фигура быть помещена выше, слева, справа или ниже головоломки
				switch ((int)(Mathf.Floor(Random.value * 4)) + 1)
				{
					case 1: // above
						r = new Rect(p.x - (s.x/2) - (dX/2) + (s.x * 0.1f) , p.y + (s.y/2) + dY + si - (dY/2) , s.x * 0.8f , -1 * (si - dY));
						break;
					case 2: // right side
						r = new Rect(p.x + (s.x / 2) + dX - (dX/2), p.y + (s.y / 2) - (s.y * 0.1f), si - dX, -1 * s.y * 0.8f);
						break;
					case 3:  // below
						r = new Rect(p.x - (s.x / 2) - (dX / 2) + (s.x * 0.1f), p.y - (s.y / 2) - (dY/2) , s.x * 0.8f, -1 * (si - dY));
						break;
					case 4:  // left side
						r = new Rect(p.x - (s.x / 2) - dX - si + (dX/2) , p.y + (s.y/2) - (s.y * 0.1f) , si - dX, -1 * s.y * 0.8f);
						break;
				}

				// поскольку мы использовали мировые координаты, мы должны перенести кусок в мир, удалив родителя
				// piece.transform.parent = null;
				// определить случайную позицию с допустимым прямоугольником
				piece.transform.parent = piecesContainer.transform;

				piece.transform.position =
					// начинаем с позиции головоломки
					transform.position +
					// перейти к x прямоугольника размещения
					transform.right * (-1 * r.xMin) +
					// добавляем случайную позицию x
					transform.right * (-1 * Random.value * r.width) +
					// перейти к верхней части прямоугольника размещения
					transform.up * r.yMin +
					// добавляем случайную позицию y
					transform.up * (Random.value * r.height) +
					// просто двигаемся "вперед" по поверхности примитива головоломки
					transform.forward * ((transform.localScale.z / 2) + 0.001f) +
					// добавить случайное значение вперед для лучшего перемещения и выбора.
					transform.forward * (0.004f + (0.001F * Random.value * 20));
						
				if (transform.parent!=null)
				{
					Vector2 vp = transform.parent.localToWorldMatrix.MultiplyPoint3x4(transform.localPosition);
					piece.transform.position -= (Vector3)vp;
				}
			
						
			}
		}

		// создать плоскость удара с помощью мыши
		private void SetPlane()
		{
			// Создать примитивный игровой объект Hit Plane для управления движением головоломки
			puzzlePlane = GameObject.CreatePrimitive(PrimitiveType.Cube);
			puzzlePlane.name = "puzzlePlane";
			// расположите, поверните и масштабируйте его в соответствии с головоломкой (масштаб x / y x10)
			puzzlePlane.transform.parent = transform;
			puzzlePlane.transform.rotation = transform.rotation;
			puzzlePlane.transform.localScale = new Vector3(10, 10, 0.0001F);
			// // пусть эта сбитая плоскость плывет просто "вперед" головоломки
			puzzlePlane.transform.position = transform.position +
			                                 transform.forward * ((transform.localScale.z / 2) + 0.0004F);
			// установите маску слоя для быстрой передачи лучей
			puzzlePlane.layer = main.LayerMask;
			// удалите средство визуализации, чтобы мы использовали только коллайдер
			Destroy(puzzlePlane.GetComponent("MeshRenderer"));
		}

		// создайте образец изображения
		private void SetSample()
		{
			// если у нас уже есть один, сначала уничтожьте его
			if (sampleImage != null) GameObject.Destroy(sampleImage);
			// создать примитивный куб
			sampleImage = GameObject.CreatePrimitive(PrimitiveType.Cube);
			sampleImage.name = "sampleImage";
			// расположите, поверните и масштабируйте его в соответствии с головоломкой ( очень тонкий)
			sampleImage.transform.parent = gameObject.transform;
			sampleImage.transform.localScale = new Vector3(1, 1, 0.0001F);
			sampleImage.transform.rotation = transform.rotation;
			sampleImage.transform.position = transform.position +
			                                 transform.forward * ((transform.localScale.z / 2) + 0.0005F);
			// установить изображение в материал головоломки для образца материала изображения (можно установить в основном классе головоломки)
			sampleImage.GetComponent<Renderer>().material = main.SampleImage;
			// установить изображение в изображение головоломки
			sampleImage.GetComponent<Renderer>().material.mainTexture = image;
			sampleImage.GetComponent<Renderer>().material.mainTextureOffset = Vector2.zero;
			sampleImage.SetActive(false);
		}
	
		// создание штучных контейнеров
		private void CreateContainers()
		{
			if (piecesContainer != null) GameObject.Destroy(piecesContainer);
			if (puzzleContainer != null) GameObject.Destroy(puzzleContainer);
			if (pieceCache != null) GameObject.Destroy(pieceCache);
		
			// Контейнер для кусочков будет содержать все "свободные" разбросанные кусочки
			piecesContainer = new GameObject("piecesContainer");
			piecesContainer.transform.parent = gameObject.transform;
			piecesContainer.transform.rotation = transform.rotation;
			piecesContainer.transform.localScale = transform.localScale;
			piecesContainer.transform.position = transform.position;
		
			// контейнер головоломки будет содержать все "размещенные" части
			puzzleContainer = new GameObject("puzzleContainer");
			puzzleContainer.transform.parent = gameObject.transform;
			puzzleContainer.transform.rotation = transform.rotation;
			puzzleContainer.transform.localScale = transform.localScale;
			puzzleContainer.transform.position = transform.position;
		
			// кэш фрагментов будет содержать все фрагменты, которые были созданы, но больше не
			// используются в текущей головоломке, но могут быть использованы повторно после изменения размера или перезапуска
			pieceCache = new GameObject("pieceCache");
			pieceCache.transform.parent = gameObject.transform;
			pieceCache.transform.rotation = transform.rotation;
			pieceCache.transform.localScale = transform.localScale;
			pieceCache.transform.position = transform.position;
		}
	
		// получить тип фигуры (9), связанный с предоставленной позицией в головоломке
		//
		// 	TL	T	TR
		//	L	C	R
		// 	BL	B	BR
		//
		private string GetType(Vector2 pos)
		{
			float x = pos.x;
			float y = pos.y;

			string pt = "C";
			if (y == 1)
			{
				if (x == 1) pt = "TL";
				else
				if (x == size.x) pt = "TR";
				else
					pt = "T";
			}
			else
			if (y == size.y)
			{
				if (x == 1) pt = "BL";
				else
				if (x == size.x) pt = "BR";
				else
					pt = "B";
			}
			else
			if (x == 1)
				pt = "L";
			else
			if (x == size.x)
				pt = "R";
			return pt;
		}

		// рассчитать правильное положение для элемента, расположенного по оси x,y на головоломке
		private Vector3 PiecePosition(Vector3 pos)
		{
			float dX = transform.localScale.x / size.x;
			float dY = transform.localScale.y / size.y;

			// определите вектор x/y, связанный с положением, для этой детали
			Vector3 positionVector =
				(transform.right * ((((transform.localScale.x / 2) * -1) + (dX * (pos.x - 1))+ (dX * (spacing/2))) * -1)) +
				(((transform.localScale.y / 2)) - (dY * (pos.y - 1)) - (dY * (spacing/2))) * transform.up;

			// установите положение части в нужное место на головоломке
			return transform.position +
			       transform.forward * ((transform.localScale.z / 2) + 0.001f) +
			       positionVector;
		}
	
		// инициализировать конкретный фрагмент с правильным масштабом, положением и текстурой (масштаб / смещение)
		private void InitPiece(GameObject puzzlePiece, Vector2 pos)
		{
		
			// если бы у нас была головоломка 5х5, мы должны были бы масштабировать части прототипа (из блендера) следующим образом
			// Вектор3 масштаб 5x5 = Вектор3.Масштаб(новый Вектор3(0,1146f, 0,1146f, 0,25f), преобразование.Локальный масштаб);
			Vector3 scale5x5 = Vector3.Scale(new Vector3(11.45f, 11.45f, 36.14547f), transform.localScale);
			// определите вектор масштаба, связанный с размером головоломки
			Vector3 CxR = new Vector3(1 / (0.2F * size.x), 1 / (0.2F * size.y), 25 / (size.x * size.y));
			// установите фигуру в мировое пространство, чтобы мы могли работать с размерами головоломки
			puzzlePiece.transform.parent = null;
			// установите правильный масштаб для фигуры в мировом пространстве
			puzzlePiece.transform.localScale = Vector3.Scale( scale5x5 * (1-spacing), CxR);    
			// вращаться, как головоломка
			puzzlePiece.transform.rotation = transform.rotation;
			// добавить деталь в контейнер
			puzzlePiece.transform.parent = piecesContainer.transform;		        
			// установите положение части в нужное место на головоломке
			puzzlePiece.transform.position = PiecePosition(pos);
			// добавьте правильный вектор положения x,y2 в позиции деталей для управления Inplace()
			piecePositions.Add(puzzlePiece.name, pos);				
			// теперь мы будем работать с локальным масштабом текстуры, так что 1 = ширина / высота головоломки.
			float scaleX = 1 / (0.2F * size.x);
			float scaleY = 1 / (0.2F * size.y);
			// установите материал поверхности / изображения на материал разбросанных частей (можно установить в основном классе Jigsaw)
			puzzlePiece.GetComponent<Renderer>().material = main.ScatteredPieces;
			// установите базовый материал детали в базовый материал детали (можно установить в основном классе Jigsaw)
			puzzlePiece.GetComponent<Renderer>().materials[1] = main.PieceBase;
			// установите текстуру поверхности для изображения головоломки
			puzzlePiece.GetComponent<Renderer>().material.mainTexture = image;
			// масштабирование текстуры поверхности
			puzzlePiece.GetComponent<Renderer>().material.mainTextureScale = new Vector2(scaleX, scaleY);
			// определите смещение текстуры, связанное с размером и положением детали
			puzzlePiece.GetComponent<Renderer>().material.mainTextureOffset = new Vector2(0.2F * scaleX * (pos.x - 1), -0.2F * scaleY * (pos.y - 1 + (5-size.y)));
		}
	
		// создайте новую деталь из определенного прототипа на определенной позиции с определенным типом детали
		private GameObject CreateNewPiece(Vector2 piece, Vector2 pos, string pType)
		{
			GameObject puzzlePiece = null;
			// получить прототип детали из основного
			Transform basePiece = main.GetPiece("" + piece.y + "" + piece.x, pType);
			if (basePiece != null)
			{
				// прототип найден, так что создайте экземпляр
				puzzlePiece = GameObject.Instantiate(basePiece.gameObject, new Vector3(pos.x * 2F, pos.y * -2F, 0), Quaternion.Euler(new Vector3(0, 180, 0))) as GameObject;
				// добавить коллайдер к кусочку головоломки
				puzzlePiece.AddComponent<BoxCollider>();
			}
			// добавить в определенный слой для быстрой передачи лучей в будущем
			puzzlePiece.layer = main.LayerMask;
			return puzzlePiece;
		}

		// Создать или установить (инициализировать) все части текущей головоломки
		private void SetPieces(bool recreate)
		{
			// у нас должна быть действительная часть
			if (topLeftPiece.Length != 2) return;
			if (size.x <= 1 || size.y <= 1) return;

			if (!recreate)
				// создавайте контейнеры только в первый раз
				CreateContainers();
			else
			{
				// удалить все активные части из головоломки
				while (pieces.Count>0)
				{
					GameObject p = pieces[0] as GameObject;
					// создание массива фигур и позиций
					pieces.Remove(p);
					piecePositions.Remove(p.name);
					p.SetActive(false);
					// добавить фрагмент в кэш для повторного использования
					p.transform.parent = pieceCache.transform;
				}
			}
		
			// определить верхнюю левую часть x и y линии
			int tpX = System.Convert.ToInt32(topLeftPiece.Substring(1, 1));
			int tpY = System.Convert.ToInt32(topLeftPiece.Substring(0, 1));
			int bX = tpX;

			int idX = 1;
			int idY = 1;
		
			neighborGrid = new GameObject[(int)size.x+1,(int)size.y+1]; //строка 0 и столбец 0 будут равны нулю
			neighborGridLookup = new Dictionary<GameObject, Vector2>();
		
			// зацикливание вертикальных рядов головоломки
			for (int y = 1; y <= size.y; y++)
			{
				// зацикливание горизонтальных столбцов головоломки
				for (int x = 1; x <= size.x; x++)
				{
					// получить тип фигуры текущей позиции
					string pType = GetType(new Vector2(x, y));
					// проверьте, была ли создана конкретная часть ранее
					GameObject puzzlePiece = piecesLookup["" + tpY + tpX + pType+ "-" + idX] as GameObject;
					if (puzzlePiece != null)
					{
						// фрагмент головоломки был создан, но не может быть активным, если это так, мы должны увеличить индекс идентификатора фрагмента
						// чтобы найти неактивный созданный фрагмент
						while (puzzlePiece!=null && puzzlePiece.activeSelf == true)
						{
							idX++;
							puzzlePiece = piecesLookup["" + tpY + tpX + pType + "-" + idX] as GameObject;
						}
					}
					if (puzzlePiece!=null)
					{
						// найден созданный фрагмент, который можно использовать
						puzzlePiece.name = "" + tpY + tpX + pType + "-" + idX;
						// добавьте кусочек головоломки к кусочкам этой головоломки
						InitPiece(puzzlePiece, new Vector2(x,y));
						pieces.Add(puzzlePiece);
						puzzlePiece.SetActive(true);
					}
					else
					{
						// создайте новый кусочек головоломки
						puzzlePiece = CreateNewPiece(new Vector2(tpX, tpY), new Vector2(x,y), pType);
						puzzlePiece.name = "" + tpY + tpX + pType + "-" + idX;
						if (puzzlePiece != null)
						{
							// добавьте кусочек головоломки к кусочкам этой головоломки и в таблицу поиска
							InitPiece(puzzlePiece, new Vector2(x, y));
							piecesLookup.Add(puzzlePiece.name, puzzlePiece);
							pieces.Add(puzzlePiece);
						}
					}
					tpX++;
					if (tpX == bX + size.x || tpX == 6)
					{
						if (tpX == 6)
						{
							tpX = 1;
							idX++;
						}
						else
							tpX = bX;
					}
					neighborGrid[x,y] = puzzlePiece;
					neighborGridLookup.Add(puzzlePiece,new Vector2(x,y));
				}
				tpX = bX;
				idX = 1;
				tpY++;
				if (tpY == 6)
				{
					tpY = 1;
					idY++;
				}
			}
		}

	}
}
